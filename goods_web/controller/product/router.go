package product

import (
	"github.com/gin-gonic/gin"
	"goods_web/middleware"
)

func Router(router *gin.RouterGroup)  {
	// 商品列表
	router.GET("/get_product_list", middleware.JwtTokenAdminValid, GetProductList)
	// 添加商品
	router.POST("/product_add", middleware.JwtTokenAdminValid, AddProduct)
	// 删除商品
	router.POST("/product_del", middleware.JwtTokenAdminValid, DeleteProduct)
	// 进入修改商品
	router.GET("/product_edit", middleware.JwtTokenAdminValid, ToEditProduct)
	// 修改商品
	router.POST("/do_product_edit", middleware.JwtTokenAdminValid, DoEditProduct)
}