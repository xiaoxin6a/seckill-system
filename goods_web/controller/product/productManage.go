package product

import (
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/micro/go-micro/v2/client/grpc"
	product "goods_product_srv/proto/product"
	"goods_web/utils"
	"net/http"
	"strconv"
	"time"
)

// 商品列表
func GetProductList(c *gin.Context) {
	// 获取参数
	currentPage := c.DefaultQuery("currentPage", "1")
	pageSize := c.DefaultQuery("pageSize", "10")

	// 调用服务
	client := grpc.NewClient()
	productsService := product.NewProductsService("go.micro.service.goods_product_srv", client)
	productsResponse, err := productsService.ProductsList(context.Background(), &product.ProductsRequest{
		CurrentPage: utils.StrToInt32(currentPage),
		PageSize:    utils.StrToInt32(pageSize),
	})

	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"msg": productsResponse.Msg,
			"code": productsResponse.Code,
		})
	}

	c.JSON(http.StatusOK, gin.H{
		"code": productsResponse.Code,
		"msg": productsResponse.Msg,
		"products": productsResponse.Products,
		"total": productsResponse.Total,
		"current_page": productsResponse.Current,
		"page_size": productsResponse.PageSize,
	})
}

// 添加商品
func AddProduct(c *gin.Context) {
	// 接收参数
	name := c.PostForm("name")
	price := c.PostForm("price")
	num := c.PostForm("num")
	uint := c.PostForm("uint")
	desc := c.PostForm("desc")
	file, err := c.FormFile("pic")

	fmt.Println(name, price, num, uint, desc, utils.StrToFloat32(price))

	client := grpc.NewClient()
	if err != nil {   // 没有上传文件
		// 调用服务
		productService := product.NewProductsService("go.micro.service.goods_product_srv", client)
		addProductResponse, _ := productService.AddProduct(context.TODO(), &product.AddProductRequest{
			Name:       name,
			Price:      utils.StrToFloat32(price),
			Num:        utils.StrToInt32(num),
			Uint:       uint,
			Pic:        "",
			Desc:       desc,
		})

		c.JSON(http.StatusOK, gin.H{
			"code": addProductResponse.Code,
			"msg": addProductResponse.Msg,
		})
	}else { // 保存文件
	    // 时间戳
	    unixTime := time.Now().Unix()
	    strUnixTime := strconv.FormatInt(unixTime, 10)
		filePath := "upload/" + strUnixTime + file.Filename
		saveUploadedFileErr := c.SaveUploadedFile(file, filePath)
		if saveUploadedFileErr != nil {
			fmt.Println("上传失败")
		}
		// fmt.Println(filePath)

		// 调用服务
		productService := product.NewProductsService("go.micro.service.goods_product_srv", client)
		addProductResponse, _ := productService.AddProduct(context.Background(), &product.AddProductRequest{
			Name:       name,
			Price:      utils.StrToFloat32(price),
			Num:        utils.StrToInt32(num),
			Uint:       uint,
			Pic:        filePath,
			Desc:       desc,
			CreateTime: time.Now().String(),
		})
		c.JSON(http.StatusOK, gin.H{
			"code": addProductResponse.Code,
			"msg": addProductResponse.Msg,
		})
	}
}

// 删除商品
func DeleteProduct(c *gin.Context) {
	// 获取参数
	id := c.PostForm("id")
	// fmt.Println(id)
	// 校验参数
	if id == "" {
		c.JSON(http.StatusOK, gin.H{
			"code": 400,
			"msg": "请求失败",
		})
	}else {
		// 调用服务
		client := grpc.NewClient()
		productsService := product.NewProductsService("go.micro.service.goods_product_srv", client)
		deleteProductResponse, _ := productsService.DeleteProduct(context.TODO(), &product.DeleteProductRequest{
			Id: utils.StrToInt32(id),
		})
		c.JSON(http.StatusOK, gin.H{
			"code": deleteProductResponse.Code,
			"msg": deleteProductResponse.Msg,
		})
	}
}

// 进入修改商品界面
func ToEditProduct(c *gin.Context)  {
	// 接收参数
	id := c.Query("id")
	// fmt.Println(id)
	client := grpc.NewClient()
	productsService := product.NewProductsService("go.micro.service.goods_product_srv", client)
	toEditProductResponse, err := productsService.ToEditProduct(context.TODO(), &product.ToEditProductRequest{Id: utils.StrToInt32(id)})

	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"code": toEditProductResponse.Code,
			"msg": toEditProductResponse.Msg,
		})
	}else {
		c.JSON(http.StatusOK, gin.H{
			"code": toEditProductResponse.Code,
			"msg": toEditProductResponse.Msg,
			"product": toEditProductResponse.Product,
			"image_base64": utils.Img2Base64(toEditProductResponse.Product.Pic),
		})
	}

}

// 修改商品
func DoEditProduct(c *gin.Context) {
	// 接收参数
	id := c.PostForm("id")
	name := c.PostForm("name")
	price := c.PostForm("price")
	num := c.PostForm("num")
	uint := c.PostForm("uint")
	desc := c.PostForm("desc")
	file, err := c.FormFile("pic")

	// fmt.Println(name, price, num, uint, desc, utils.StrToFloat32(price))

	client := grpc.NewClient()
	if err != nil {   // 没有上传文件
		// 调用服务
		productService := product.NewProductsService("go.micro.service.goods_product_srv", client)
		doProductResponse, _ := productService.DoEditProduct(context.TODO(), &product.DoEditProductRequest{
			Id:    utils.StrToInt32(id),
			Name:  name,
			Price: utils.StrToFloat32(price),
			Num:   utils.StrToInt32(num),
			Uint:  uint,
			Pic:   "",
			Desc:  desc,
		})

		c.JSON(http.StatusOK, gin.H{
			"code": doProductResponse.Code,
			"msg": doProductResponse.Msg,
		})
	}else { // 保存文件
		// 时间戳
		unixTime := time.Now().Unix()
		strUnixTime := strconv.FormatInt(unixTime, 10)
		filePath := "upload/" + strUnixTime + file.Filename
		saveUploadedFileErr := c.SaveUploadedFile(file, filePath)
		if saveUploadedFileErr != nil {
			fmt.Println("上传失败")
		}
		// fmt.Println(filePath)

		// 调用服务
		productService := product.NewProductsService("go.micro.service.goods_product_srv", client)
		doProductResponse, _ := productService.DoEditProduct(context.Background(), &product.DoEditProductRequest{
			Id:			utils.StrToInt32(id),
			Name:       name,
			Price:      utils.StrToFloat32(price),
			Num:        utils.StrToInt32(num),
			Uint:       uint,
			Pic:        filePath,
			Desc:       desc,
		})
		c.JSON(http.StatusOK, gin.H{
			"code": doProductResponse.Code,
			"msg": doProductResponse.Msg,
		})
	}
}