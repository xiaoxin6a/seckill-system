package seckill

import (
	"github.com/gin-gonic/gin"
	"goods_web/middleware"
)

func Router(router *gin.RouterGroup)  {
	/*
	管理端
	 */
	// 活动列表
	router.GET("/get_seckill_list", middleware.JwtTokenAdminValid, GetSeckillList)
	// 获取所有的商品
	router.GET("/get_products", GetProducts)
	// 添加活动
	router.POST("/seckill_add", middleware.JwtTokenAdminValid, AddSeckill)
	// 编辑活动显示数据
	router.GET("/seckill_to_edit", middleware.JwtTokenAdminValid, ToEditSeckill)
	// 编辑活动数据
	router.POST("/seckill_do_edit", middleware.JwtTokenAdminValid, DoEditSeckill)
	// 删除活动
	router.POST("/seckill_del", middleware.JwtTokenAdminValid, DeleteSeckill)



	/*
	前端展示
	 */
	router.GET("/front/get_seckill_list", FrontGetSeckillList)
	/*
	前端活动详细页面
	 */
	router.GET("/front/seckill_detail", middleware.JwtTokenFrontValid, FrontSeckillDetail)


	/*
	秒杀实现
	 */
	router.POST("/front/seckill", middleware.JwtTokenFrontValid, Seckill)

	// 获取响应结果
	router.GET("/front/get_seckill_result", middleware.JwtTokenFrontValid, GetResult)
}