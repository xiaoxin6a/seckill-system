package user

import (
	"github.com/gin-gonic/gin"
	"goods_web/middleware"
)



func Router(router *gin.RouterGroup)  {
	// 发送邮件验证码
	router.POST("/send_email", SendEmail)
	// 注册
	router.POST("/front_user_register", Register)

	// 测试生成token
	router.GET("/testGenToken", TestGenToken)
	// 测试验证token
	router.GET("/testValidToken", TestValidToken)


	// 用户登录
	router.POST("/front_user_login", FrontLogin)

	// 管理员登陆
	router.POST("/admin_login", AdminLogin)
	// 管理员端用户列表
	router.GET("/get_front_users", middleware.JwtTokenAdminValid, AdminUserList)
}