package user

import (
	"context"
	"github.com/gin-gonic/gin"
	"github.com/micro/go-micro/v2/client/grpc"
	"goods_web/utils"
	"log"
	"net/http"

	front_user "goods_user_srv/proto/front_user"
)

func SendEmail(c *gin.Context) {
	email := c.PostForm("email")
	// log.Println(utils.VerifyEmail(email))


	ok := utils.VerifyEmail(email)
	if ok {
		// 手动创建client
		client := grpc.NewClient()
		// 调用用户服务
		userSrvService := front_user.NewFrontUserService("go.micro.service.goods_user_srv", client)
		response, err := userSrvService.FrontUserEmail(context.TODO(), &front_user.FrontUserEmailRequest{
			Email: email,
		})
		// log.Println(response.Msg)
		if err != nil {
			c.JSON(http.StatusOK, gin.H{
				"code": response.Code,
				"msg": response.Msg,
			})
		}else {
			c.JSON(http.StatusOK, gin.H{
				"code": response.Code,
				"msg": response.Msg,
			})
		}
		
	}else {
		c.JSON(http.StatusOK, gin.H{
			"code": 500,
			"msg": "邮箱格式不正确",
		})
	}

	log.Println(email)
}


func Register(c *gin.Context) {
	// 接收参数
	email := c.PostForm("email")
	captche := c.PostForm("captche")
	password := c.PostForm("password")
	repasswprd := c.PostForm("repassword")
	log.Println(email, captche, password, repasswprd)
	// 校验参数
	ok := utils.VerifyEmail(email)
	if ok {
		if password == repasswprd {
			// 调用用户注册服务
			// 手动创建client
			client := grpc.NewClient()
			// 调用用户服务
			userSrvService := front_user.NewFrontUserService("go.micro.service.goods_user_srv", client)
			response, err := userSrvService.FrontUserRegister(context.TODO(), &front_user.FrontUserRequest{
				Email:      email,
				Code:       captche,
				Password:   password,
				Repassword: repasswprd,
			})
			if err != nil {
				c.JSON(http.StatusOK, gin.H{
					"code": response.Code,
					"msg": response.Msg,
				})
			}else {
				c.JSON(http.StatusOK, gin.H{
					"code": response.Code,
					"msg": response.Msg,
				})
			}
		}else {
			c.JSON(http.StatusOK, gin.H{
				"code": 500,
				"msg": "输入的两次密码不正确",
			})
		}
	}else {
		c.JSON(http.StatusOK, gin.H{
			"code": 500,
			"msg": "邮箱格式不正确",
		})
	}
}


func FrontLogin(c *gin.Context) {
	// 接收参数
	email := c.PostForm("mail")
	password := c.PostForm("password")
	// 校验参数 密码校验应该在用户服务里执行
	// 这里只需要检测是否接收到值以及邮箱是否正确
	ok := utils.VerifyEmail(email)
	if ok {
		// grpc调用用户服务
		// 创建grpc连接
		client := grpc.NewClient()
		userService := front_user.NewFrontUserService("go.micro.service.goods_user_srv", client)
		frontUserLoginResponse, _ := userService.FrontUserLogin(context.Background(), &front_user.FrontUserLoginRequest{
			Email:    email,
			Password: password,
		})
			// 生成token
			token, err := utils.GenToken(frontUserLoginResponse.UserName, utils.FrontUserTokenExpireDuration, utils.FrontUserSecretKey)
			if err != nil {
				c.JSON(http.StatusOK, gin.H{
					"code": frontUserLoginResponse.Code,
					"msg": frontUserLoginResponse.Msg,
				})
			}else {
				c.JSON(http.StatusOK, gin.H{
					"code": frontUserLoginResponse.Code,
					"username": frontUserLoginResponse.UserName,
					"token": token,
					"msg": frontUserLoginResponse.Msg,
				})
			}
	}else {
		c.JSON(http.StatusOK, gin.H{
			"code": 500,
			"msg": "邮箱格式不正确",
		})
	}

}