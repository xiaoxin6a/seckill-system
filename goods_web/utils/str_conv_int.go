package utils

import (
	"strconv"
	"unsafe"
)

func StrToInt32(str string) int32  {

	valueInt64,_ := strconv.ParseInt(str,10,64)  // 字符串转64位整数
	valueInt := *(*int32)(unsafe.Pointer(&valueInt64))    // 64 -》 32
	return valueInt
}
