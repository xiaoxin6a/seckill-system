package utils

import "time"

func StrTimeToTime(str string) time.Time {
	const layOut = "2006-01-02 15:04:05"  //时间常量
	loc, _ := time.LoadLocation("Asia/Shanghai")
	res ,_ := time.ParseInLocation(layOut, str ,loc)
	return res
}