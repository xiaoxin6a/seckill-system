package models

import "time"

/*
商品模型类
*/
type Product struct {
	Id         int     // 主键
	Name       string  // 商品名称
	Price      float32 `gorm:"type:decimal(11,2)"` // 价格
	Num        int     // 数量
	Uint       string  // 商品单位
	Pic        string  // 商品图片
	Desc       string  // 描述
	CreateTime time.Time

	SecKills []SecKills `gorm:"foreignKey:PId;references:Id"`
}

// 指定表名
func (Product) TableName() string {
	return "product"
}
