package main

import (
	"github.com/micro/go-micro/v2"
	log "github.com/micro/go-micro/v2/logger"
	"github.com/micro/go-micro/v2/registry"
	"github.com/micro/go-micro/v2/registry/etcd"
	"goods_product_srv/controller"
	_ "goods_product_srv/data_source"
	product "goods_product_srv/proto/product"
	seckill "goods_product_srv/proto/seckill"
)

func main() {
	// New Service
	service := micro.NewService(
		micro.Name("go.micro.service.goods_product_srv"),
		micro.Version("latest"),
		micro.Registry(etcd.NewRegistry(
			registry.Addrs("127.0.0.1:2379"),
			)),
	)

	// Initialise service
	service.Init()

	// 注册服务
	product.RegisterProductsHandler(service.Server(), new(controller.Products))

	seckill.RegisterSeckillsHandler(service.Server(), new(controller.Seckills))




	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
