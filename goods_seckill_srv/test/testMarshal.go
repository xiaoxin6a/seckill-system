package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"goods_seckill_srv/utils"
)

func main()  {
	data := map[string]interface{}{
		"email": "123@qq.com",
		"id": 6,
	}
	fmt.Printf("%v %T %v %T\n", data["email"],data["email"], data["id"], data["id"])  // 123@qq.com string 6 int

	data2, _ := json.Marshal(data)
	fmt.Printf("%v %T\n", string(data2), data2)

	var data3 map[string]interface{}
	json.Unmarshal(data2, &data3)
	fmt.Printf("%v %T %v %T\n", data3["email"],data3["email"], data3["id"], data3["id"])  // 123@qq.com string 6 float64
	
	
	/*
	使用decoder去反序列化
	 */
	var data4 map[string]interface{}
	decoder := json.NewDecoder(bytes.NewReader(data2))
	decoder.UseNumber()
	err := decoder.Decode(&data4)
	if err != nil {
		fmt.Printf("unmarshal failed, err:%v\n", err)
		return
	}
	fmt.Printf("%v %T %v %T\n", data4["email"],data4["email"], data4["id"], data4["id"])

	// 将m2["count"]转为json.Number之后调用Int64()方法获得int64类型的值
	id, err := data4["id"].(json.Number).Int64()
	if err != nil {
		fmt.Printf("parse to int64 failed, err:%v\n", err)
		return
	}
	fmt.Printf("type:%T\n", int(id)) // int


	res := utils.StringToMap(data2)
	fmt.Printf("%v %T %v %T\n", res["email"],res["email"], res["id"], res["id"])
}