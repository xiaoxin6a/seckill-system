# seckill-system

#### 介绍

#### 软件架构
开发模式：前后端分离

前端：vue3、antdv、vue-router、axios

后端：gin框架、gorm、go-micro/v2

服务注册：etcd

后端服务：数据存储服务mysql、缓存服务redis、消息队列rabbitMQ


#### 安装教程

1.  安装vue3
2.  安装gin框架  

    `go get -u github.com/gin-gonic/gin`
3.  安装微服务相关 

      `go get github.com/micro/go-micro/v2`
      
       `go get github.com/micro/micro/v2`
       
      `go get github.com/micro/protoc-gen-micro/v2`
      
4.  go安装RabbitMQ客户端库

    `go get github.com/streadway/amqp`
    
5.  go安装Redis客户端库(go版本为1.14以下，高版本应安装v8版本)

      `go get -u github.com/go-redis/redis`

#### 项目搭建说明

1.  vue create 项目名  创建vue项目
2.  micro new 服务名 --namespace=""   创建服务
3.  protoc -I . --micro_out=. --go_out=. ./xxx.proto   生成文件

#### 项目使用说明
> git clone 项目

1.  开启go的modules功能
2.  在每个服务包下使用go mod tidy安装相关环境
3.  打开etcd服务
4.  打开redis服务
5.  打开rabbitmq服务
6.  运行

    两种方式 可以使用go run main.go启动每个服务，也可以使用以写好的bat文件启动
7.  若想要启动web界面查看已注册的服务，则运行web服务下的web.bat

#### 已实现的功能

1.  用户服务--普通用户的登陆注册以及管理端的登陆，管理端用户列表的展示
2.  商品服务--管理端商品的增删改查，秒杀活动内容的增删改查
              前端活动的展示以及详细活动展示
3.  秒杀服务--使用消息队列加缓存实现简单的秒杀过程
4.  跨域解决
5.  JWT认证
6.  自定义日志中间件

#### 待实现功能
1.  秒杀升级-支付功能
2.  图片存储使用腾讯云对象存储
3.  ......

#### 前端代码
[秒杀系统前端代码](https://gitee.com/wear-crown/seckill-system-front)
