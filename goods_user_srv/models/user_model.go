package models

import "time"

/*
用户模型类
 */
type User struct {
	Id int `gorm:"type:int(10);AUTO_INCREMENT"`
	Email string `gorm:"type:varchar(64)"`
	Password string `gorm:"type:varchar(64)"`
	Desc string `gorm:"type:varchar(255)"`
	Status int `gorm:"type:int(2)"`
	CreateTime time.Time
}

// 指定表名
func (User) TableName() string {
	return "front_user"
}