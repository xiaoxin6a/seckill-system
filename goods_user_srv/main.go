package main

import (
	"github.com/micro/go-micro/v2"
	log "github.com/micro/go-micro/v2/logger"
	"github.com/micro/go-micro/v2/registry"
	"github.com/micro/go-micro/v2/registry/etcd"
	"goods_user_srv/controller"
	admin_user "goods_user_srv/proto/admin_user"
	front_user "goods_user_srv/proto/front_user"

	_ "goods_user_srv/data_source"
)

func main() {
	// New Service
	service := micro.NewService(
		micro.Name("go.micro.service.goods_user_srv"),
		micro.Version("latest"),
		micro.Registry(etcd.NewRegistry(
			// 地址是本地etcd地址
			registry.Addrs("127.0.0.1:2379"),
			)),
	)

	// Initialise service
	service.Init()

	// Register Handler
	// goods_user_srv.RegisterGoodsUserSrvHandler(service.Server(), new(handler.Goods_user_srv))
	front_user.RegisterFrontUserHandler(service.Server(), new(controller.FrontUser))

	admin_user.RegisterAdminUserHandler(service.Server(), new(controller.AdminUser))

	// Register Struct as Subscriber
	//micro.RegisterSubscriber("go.micro.service.goods_user_srv", service.Server(), new(subscriber.Goods_user_srv))

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
